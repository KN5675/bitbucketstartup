﻿using System.Windows;

namespace CalorieCounter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window 
    {
    //Defining the calorie count for each fruit and setting the calorie tally to zero
        double caloriesSoFar = 0;
        const int APPLE_CALORIES = 80;
        const int BANANA_CALORIES = 115;
        const int ORANGE_CALORIES = 90;
        const int PEAR_CALORIES = 120;
        public MainWindow()
        {
            InitializeComponent();
        }
    //Setting the Button click event
        private void ButtonApple_Click(object sender, RoutedEventArgs e)
        {
            caloriesSoFar += APPLE_CALORIES;
            //sending vaue to diaplay in textblock
            TextBlockTotalCalories.Text = caloriesSoFar.ToString("0.00");
        }
    //Setting the Button click event
        private void ButtonBananna_Click(object sender, RoutedEventArgs e)
        {
            caloriesSoFar += BANANA_CALORIES;
            TextBlockTotalCalories.Text = caloriesSoFar.ToString("0.00");
        }
    //Setting the Button click event
        private void ButtonOrange_Click(object sender, RoutedEventArgs e)
        {
            caloriesSoFar += ORANGE_CALORIES;
            TextBlockTotalCalories.Text = caloriesSoFar.ToString("0.00");
        }
    //Setting the Button click event
        private void ButtonPear_Click(object sender, RoutedEventArgs e)
        {
            caloriesSoFar += PEAR_CALORIES;
            TextBlockTotalCalories.Text = caloriesSoFar.ToString("0.00");
        }
    //Setting the Button click event
        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            TextBlockTotalCalories.Text = "0";
            caloriesSoFar = 0;
        }
    }
}

