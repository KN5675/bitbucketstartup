Instructions for CalorieCounter.sln

Open project with Microsoft Visual studio and press play to run the app. Click the picture of each fruit you would
like to add to the total calorie count. Watch the total calories output on the right hand side of the screen increase
with each fruit you select. When your desired calorie tally has been reached press the reset button, to clear the total 
back to zero.

Explination For License Choice

I have chosen Open Software License v. 3.0 (OSL-3.0. This license allows anyone to use, adapt, 
reproduce, display, or perform this software. The reason this license was chosen is because this app is very simple. 
There are no special details to protect, it is a rudimentry application created in the spirit of learning and sharing. 
I want others to be able to use and learn from it for free and have the ability to add or change it to facilitate 
their learning process.
